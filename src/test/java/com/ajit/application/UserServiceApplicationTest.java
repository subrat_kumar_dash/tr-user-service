package com.ajit.application;


import com.ajit.UserServiceApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(classes = UserServiceApplication.class)
public class UserServiceApplicationTest {

    @Test
    public void test() {

    }
}
