package com.ajit.processor;

import com.ajit.info.EmployeeResponse;
import com.ajit.model.Employee;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;

@Service
public class EmployeeProcessor {

    RestTemplate restTemplate = new RestTemplate();

    public void callReceiverCreate(Employee employee, HttpServletRequest request) throws Exception {
        try {
            HttpEntity requestEntity = getEmployeeRequest(employee, request);
            String receiverEndPoint = "http://localhost:8084/v1/receiver";
            EmployeeResponse employeeResponse = restTemplate.exchange(receiverEndPoint, HttpMethod.POST, requestEntity, EmployeeResponse.class).getBody();
            if (!ObjectUtils.isEmpty(employeeResponse.getErrorResponse())) {

            } else {
                employeeResponse.setMessage("Employee Successful created");
            }
        } catch (Exception e) {
            throw new Exception("Employee record not created");
        }
    }

    private HttpEntity getEmployeeRequest(Employee employee, HttpServletRequest request) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("fileType", request.getHeader("fileType"));
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(employee, headers);
    }

}
