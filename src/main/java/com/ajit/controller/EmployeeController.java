package com.ajit.controller;

import com.ajit.EncryDecrypt;
import com.ajit.exception.DataNotFoundException;
import com.ajit.exception.RecordNotFoundException;
import com.ajit.info.EmployeeResponse;
import com.ajit.model.Employee;
import com.ajit.processor.EmployeeProcessor;
import com.ajit.service.EmployeeService;
import com.ajit.util.EmployeeUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/v1/user")
public class EmployeeController {

	@Autowired
	EmployeeService employeeService;

	@Autowired
	EmployeeUtil employeeUtil;

	@Autowired
	EmployeeProcessor employeeProcessor;


	/**
	 *
	 * @param employee
	 * @param request
	 * @return responseEntity
	 * @throws IOException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity createEmployee(@RequestBody Employee employee, HttpServletRequest request) {
		EmployeeResponse resp = new EmployeeResponse<>();
		//String secretKey = request.getHeader("secretKey");
		//Object employee = request.getHeader("employee");
		//String str = EncryDecrypt.decrypt(employee,secretKey);

		//Employee employee1 = new ObjectMapper().readValue(str, Employee.class);

		//ObjectMapper mapper = new ObjectMapper();
		//Employee employee1 = mapper.readValue(str, Employee.class);

		if(ObjectUtils.isEmpty(employee)) {
			throw new RecordNotFoundException("Employee data not be empty");
		}

		//Validate Employee
		validateEmployeeData(employee);

		String type = request.getHeader("fileType");
		try {
			if(type.equalsIgnoreCase("csv")) {
				resp.setData(employeeService.save(employee));
			} else if(type.equalsIgnoreCase("xml")) {

			} else {
				throw new Exception("Invalid file type");
			}
			resp.setMessage("Employee record created successfully");

			//Call receiver microservice to create employee
			employeeProcessor.callReceiverCreate(employee, request);
			return ResponseEntity.status(HttpStatus.CREATED).body(resp);
		} catch (Exception e) {
			resp.setData("Invalid data");
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(resp);
		}
	}

	/**
	 *
	 * @param employee
	 * @param request
	 * @return responseEntity
	 */
	@PutMapping(value="/update", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity updateEmp(@RequestBody Employee employee, HttpServletRequest request) {
		EmployeeResponse employeeResponse = new EmployeeResponse();
		try {
			if(ObjectUtils.isEmpty(employee)) {
				employeeResponse.setErrorResponse(employeeUtil.getErrorResponse(HttpStatus.BAD_REQUEST.value(), "Input data not correct"));
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(employeeResponse);
			}
			employeeService.update(employee);
			employeeResponse.setMessage("Update successfully");
			employeeResponse.setData(employee);
			return ResponseEntity.ok(employeeResponse);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(employeeResponse);
		}

	}

	/**
	 *
	 * @return
	 */
	@GetMapping(value="/employees", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity getEmployee() {
		EmployeeResponse employeeResponse = new EmployeeResponse();
		try {
			List<Employee> employees = employeeService.findAllEmployee();
			if(CollectionUtils.isEmpty(employees)) {
				throw new RecordNotFoundException("No employee record exist");
			}
			employeeResponse.setMessage("Employee records found");
			employeeResponse.setData(employees);
			return ResponseEntity.ok(employeeResponse);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(employeeResponse);
		}
	}

	/**
	 *
	 * @param empId
	 * @return responseEntity
	 */
	@GetMapping(value="/employee/{empId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity getEmployeeById(@PathVariable("empId") String empId) {
		EmployeeResponse employeeResponse = new EmployeeResponse();
		try {
			Employee employee = employeeService.findByEmpId(empId);
			if(ObjectUtils.isEmpty(employee)) {
				employeeResponse.setMessage("No employee record found");
				employeeResponse.setErrorResponse(employeeUtil.getErrorResponse(HttpStatus.BAD_REQUEST.value(), "Input data not found"));
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body(employeeResponse);
			}
			employeeResponse.setMessage("Employee data found");
			employeeResponse.setData(employee);
			return ResponseEntity.ok(employeeResponse);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(employeeResponse);
		}
	}

	private void validateEmployeeData(Employee employee) {
		if(null == employee.getName() && StringUtils.isEmpty(employee.getName())) {
			throw new DataNotFoundException("Employee name should not be empty");
		}
		if(employee.getAge() == 0) {
			throw new DataNotFoundException("Employee age should not be empty");
		}
		if(null == employee.getDob() && StringUtils.isEmpty(employee.getDob())) {
			throw new DataNotFoundException("Employee date of birth should not be empty");
		}
		if(null == employee.getSalary() && StringUtils.isEmpty(employee.getSalary())) {
			throw new DataNotFoundException("Employee salary should not be empty");
		}
	}


}
