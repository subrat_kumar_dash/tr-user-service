package com.ajit.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;

@Data
@Component
@Document(collection="employeeAccount")
public class Employee {

	@Id
	private String empId;
	private String name;
	private String dob;
	private String salary;
	private int age;
	
	public Employee() {
		
	}
	
	public Employee(String empId, String name, String dob, String salary, int age) {
		super();
		this.empId = empId;
		this.name = name;
		this.dob = dob;
		this.salary = salary;
		this.age = age;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getSalary() {
		return salary;
	}
	public void setSalary(String salary) {
		this.salary = salary;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}


	
	
	
}
